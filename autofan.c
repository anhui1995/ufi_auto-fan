/*
#需要用到GCC编译器，安装GCC
sudo apt update
sudo apt upgrade
sudo apt install build-essential
sudo apt install manpages-dev

编译命令是
gcc -o fan autofan.c    
fan是可执行文件的名字，就是编译好要运行的那个文件，autofan.c是要进行编译的文件名

可以使用nohup <命令> & 放到/etc/rc.local中，实现开机自启



*/

/*
使用板子正面的esim的GND作为PWM输出，接在芯片下面最大的散热焊盘上就行.
使用P-MOS作为风扇驱动
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#define MAX_LINE_LEN 32
#define TEMP_PATH "/sys/devices/virtual/thermal/thermal_zone0/temp"
#define CTRL_PATH "/sys/devices/virtual/thermal/thermal_zone0/temp"


// 设置风扇速度，分为5个等级，可以自行设置
/*通过设置定时器中开和关的时间模拟PWM，
delay_on和delay_off中分别存放时间，单位ms，范围0-很大很大

*/
void setFan(int i){

    // 每次都配置触发模式，保证触发模式正确
    system("echo timer > /sys/class/leds/sim:en2/trigger");

    switch(i){
        case 0:{
            system("echo 0 > /sys/class/leds/sim:en2/delay_on");
            system("echo 500 > /sys/class/leds/sim:en2/delay_off");
        }break;

        
        case 1:{
            system("echo 1 > /sys/class/leds/sim:en2/delay_on");
            system("echo 10 > /sys/class/leds/sim:en2/delay_off");
        }break;

        
        case 2:{
            system("echo 1 > /sys/class/leds/sim:en2/delay_on");
            system("echo 1 > /sys/class/leds/sim:en2/delay_off");
        }break;

        
        case 3:{
            system("echo 30 > /sys/class/leds/sim:en2/delay_on");
            system("echo 1 > /sys/class/leds/sim:en2/delay_off");
        }break;


        case 4:{
            system("echo 500 > /sys/class/leds/sim:en2/delay_on");
            system("echo 0 > /sys/class/leds/sim:en2/delay_off");
        }break;


    }
    
}

int main(void)
{
    int fd;
    char buf[MAX_LINE_LEN];
    double temp = 0;

    setFan(0);
    while(1)
    {
        fd = open(TEMP_PATH, O_RDONLY);
        if (fd < 0)
        {
            printf("open error!\n");
            return -1;
        }

        if(read(fd, buf, MAX_LINE_LEN) < 0)
        {
            printf("read error!\n");
            return -1;
        }

        temp = ((double)atoi(buf))/1000;
        // printf("CPU temperature: %.2f\n", temp);
        // 手动设置温度和风扇转速的关系
        // 现在的不太合理，会出现反复横跳的情况，
        // 可以自行修改这里, 比如风扇状态不变的话就不用重复设置了
        if(temp < 40) setFan(0);
        else if(temp < 50) setFan(1);
        else if(temp < 60) setFan(2);
        else if(temp < 70) setFan(3);
        else if(temp < 80) setFan(4);

        // 延时1S
        sleep(1);
    }

    return 0;
}
