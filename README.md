# UFI_autoFan

#### 介绍

随身wifi系统自动温控



#### 使用方法

需要用到git，如果没有，使用apt install git安装

需要用到gcc，如果没有，也进行安装安装

sudo apt update

sudo apt upgrade

sudo apt install build-essential

sudo apt install manpages-dev



##### 下载源码

git clone https://gitee.com/anhui1995/ufi_auto-fan.git

##### 挪个地

cd ufi_auto-fan/

mkdir /fanctrl

cp autofan.c /fanctrl/

cd /fanctrl

##### 编译

gcc -o autofan autofan.c



##### 运行

这个运行会一直在前台运行，占用终端，终端关闭后回停止运行

./autofan

或者

/fanctrl/autofan



##### 后台运行

nohup ./autofan &

或者

nohup /fanctrl/autofan &



##### 设置开机自启

编辑/etc/rc.local将

nohup /fanctrl/autofan &

放到exit 0前面，实现开机自启



#### 硬件连接

使用板子正面的esim的GND作为PWM输出，接在芯片下面最大的散热焊盘上就行.

使用P-MOS作为风扇驱动

具体连接参考硬件连接文件

风扇风速和温度对应关系那里可以根据自己的需要改